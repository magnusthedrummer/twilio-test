# Twilio Coding Exercise - iOS
Author: Magnus Martikainen

This repo contains my implementation of the sample iOS coding exercise for Twilio.

## Building
To build the libraries and the sample app, simply open the workspace in xcode, select the appropriate target and build it.

The exercise was implemented using xcode 10.2.

## Running the App

In xcode, select the TwContactsSampleSwift target and run it.

## Running the Tests

In xcode, select the TwContacts target and run the tests.

## Implementation Notes

This section contains my notes on the concepts, APIs and implementations.

### Contact ID

The data provided with the exercise lacks a vital property that would normally be supplied by a server, a unique ID for each contact.

Without assigning a unique ID to each contact there is no good way to support updates.
For the purpose of this exercise I decided to use the current collection size as the ID for the next added contact. This works OK for this sample, but will break when we implement deleting of contacts.

### Immutable Contacts

With a real server back-end I expect the data would be accessed through CRUD operations on a RESTful API with JSON payloads. If so, the same contact instance may be parsed from different payloads, e.g. when the contact is loaded, re-loaded, and updated.

With this in mind I decided to make contact instances immutable. This also makes thread safety much easier to achieve, as we don't need to worry about locking the object while reading its properties.


### Loading Contacts

In this simplified implementation all contacts are loaded in one go.
With a real server back-end, loading contacts would be a paginated operation, typically triggered on-demand as the user browses the contact list. 

A paginated contact loading API will be quite a bit more complicated.

A real library implementation would likely store cached copies of the loaded contacts in a local database, to provide fast access, offline support and avoid unnecessary network operations.

If so, there would also need to be a mechanism to update that cache when contacts have changed on the server, also when the device is offline. 


### API Documentation

The public methods, properties and types are documented in the source code using `///` style doc-comments, which automatically generates context sensitive documentation in xcode.


### The C++ API

The C++ API is designed for speed and flexibility.

A namespace `tw` is used throughout the library, to avoid name clashes when integrated with code from other sources.

There is explicitly no singleton. However the `contacts_lib` and `contacts_repository` are both designed so that they could easily be used as singletons. But that is left as a choice to the API consumer.

An observer pattern is used, rather than a delegate, to provide the flexibility of using dedicated observers specialized for explicit tasks.

The C++ API is designed to be fully thread safe. 
Mutexes are used to protect against simultaneous read/updates on different threads. 
A reasonable effort has been made to hold the mutex for as short time as possible. The exception is the notification methods for the observers, where I've opted to lock the mutex while the observers are invoked. This could potentially lead to threading problems (deadlocks and/or poor performance) if observer implementations perform complex actions directly in the handlers. 

The object model is intentionally not designed for extensibility through inheritance, in order to improve speed and simplify the implementation. Extensibility through containment is preferred for the expected use cases.

Memory management is handled exclusively using `shared_ptr`. Circular references are not expected to be a problem with this hierarchical object model. To avoid unintentional circular references, observers are also not held by reference count. This puts a burden on the API client to ensure that observers are explicitly removed before they are destroyed.

`load_json_contacts`, `add` and `update` are all designed to have asynchronous implementations. In real life these methods would return immediately and the response is delivered at a later point by invoking the callback. In a server-backed implementation the callbacks get invoked when a response is received from the server or if the server call fails, both which could take a long time. Forcing an asynchronous API usage for these operations helps avoid blocking threads unnecessarily while waiting for server responses.

Both callbacks and observers are invoked when these operations are successful. When there is a failure only the callback is invoked. 
In real life all exceptions must be trapped when handling the server response on the callback thread. This is both to avoid crashing the thread and to ensure that an error gets propagated to the callback.

The library uses only cross-platform data types and functionality provided by the Standard C++ library, meaning it should compile on most plaforms without issues. The code uses C++ 11 features and exceptions. This may require compiler options to be customized when building for other platforms, including Android NDK.  
A makefile would need to be set up. 

For JSON parsing I selected the open source `json11` library, for ease of use and small footprint. (MIT license.)

Naming conventions consistent with the Standard C++ library is used throughout the C++ API, i.e. all lowercase names with underscore word separators, no prefixes. 

The API uses and handles errors and exceptions using `runtime_error` from the C++ Standard library.


### The Objective C API

The Objective C API is designed for easy and natural consumption in a typical iOS application written in Swift or Objective C. 

Typical Objective C naming conventions is used in this API. All public interfaces are prefixed with `Tw` to avoid name clashes with other libraries. 
No prefix is used for the corresponding Swift names, since in Swift the fully qualified name for types in the imported framework includes the framework name, which can be used to disambiguate if necessary (e.g. `TwContacts.Contact` instead of just `Contact`).
For good Swift support, nullability has been explicitly declared in all the public interfaces and explicit Swift names have been assigned to methods and properties to follow Swift naming conventions.
One additional change I would have wanted to make for Swift is to expose `ContactsManager.shared` as a static property rather than as a static method, but I could not find a good way to do that.

A singleton `TwContactsManager.sharedManager` provides convenient access to the shared repository with a thread safe implementation.
All changes to the repository content can be handled by providing a `TwContactsManagerDelegate`. This is a pattern familiar to iOS developers, making the library easy to use.
In real life, the underlying implementation invokes underlying callbacks on a potentially different thread. The framework is designed to invoke the delegate (and callbacks) on that thread too, but it could alternatively dispatch to the main thread first. Doing so would impact performance, and could cause some unexpected threading problems, and it is questionable whether a library should be doing that, but overall it would likely also reduce the number of app bugs. It could be a trade-off worth discussing.

Loading, adding and updating contacts are exposed asynchronous operations with optional callbacks, for the reasons explained in the C++ API section above. 
Note that successful operations invoke both the callbacks and the delegate, while failures invoke the callback only. This could lead to some confusion for API consumers, as they could be receiving the same contact changes twice if they implement both mechanisms.

Errors are returned as proper `NSError` instances in the callbacks.   

One property that is conspicuously missing is `TwContactsManager.contacts`. The collection of contacts is currently only accessible from the callback / delegate invoked after successful loading. This is an implementation choice that avoids the need to cache contact instances in the Objective C API implementation, or to marshal contact instances every time that property is retrieved. However it is not the most user friendly choice, and I would consider adding that property. If so, caching the contacts would likely be the better implementation choice. 

In interfacing with the C++ API it is assumed that the native strings are UTF-8, and that NSInteger is large enough to hold any native value that might be used.

A prudent implementation might also wrap all public framwork method and property implementations in `try`/`catch(...)` to ensure that exceptions thrown in the native code never escapes the framework, since native exceptions cannot be handled in Swift. (The Swift `do/try/catch` construct does not handle native exceptions, it is just syntactic sugar for `NSError**` semantics.) I prefer to do this only when there are expected failures though.

For distribution, the Objective C framework should probably be wrapped up in a Cocoapod. This would require a license file.


### The Unit Tests

The unit tests cover the basic use cases for all public API methods in the Objective C API.

Over time I would also suggest to add:

 - Explicit thread safety tests

 - Unit tests for the C++ API

 - Performance tests
 
 
 ### The Swift Sample App
 
 The sample app was intentionally implemented as bare-bones as reasonably possible, essentially by generating a new master/details app and changing it to load/add/show contacts using the `ContactManager`.
 
 In fact, the sample app is so bare bones that it still has the out-of-the-box implementation for removing items. Works fine in the app, but does not trigger any framework code, so that could be misleading.
 
The sample app implements contact add by adding random contact details. It does not implement contact updates. It could be worth extending the app do support that too.
