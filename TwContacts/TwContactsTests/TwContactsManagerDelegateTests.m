//
//  TwContactsManagerDelegateTests.m
//  TwContactsTests
//
//  Created by Magnus Martikainen on 4/1/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TwContactsManagerDelegate.h"
#import "TwContactsManager.h"
#import "TwContact.h"

/// Helper class for testing the delegate
@interface DelegateImpl : NSObject <TwContactsManagerDelegate>
@property (atomic) NSInteger loadedCalls;
@property (atomic) NSInteger addedCalls;
@property (atomic) NSInteger updatedCalls;
@property (nullable, atomic, strong) TwContact * lastAdded;
@property (nullable, atomic, strong) TwContact * lastUpdated;
@property (nullable, atomic, strong) NSArray<TwContact *> * lastLoaded;

-(void)manager:(nonnull TwContactsManager*)manager didAddContact:(nonnull TwContact* )contact;
-(void)manager:(nonnull TwContactsManager*)manager didUpdateContact:(nonnull TwContact*)contact;
-(void)manager:(nonnull TwContactsManager*)manager didLoadContacts:(nonnull NSArray<TwContact *> *) contacts;

@end
@implementation DelegateImpl
@synthesize loadedCalls, addedCalls, updatedCalls, lastAdded, lastUpdated, lastLoaded;

-(void)manager:(nonnull TwContactsManager*)manager didAddContact:(nonnull TwContact* )contact {
    ++self.addedCalls;
    self.lastAdded = contact;
}

-(void)manager:(nonnull TwContactsManager*)manager didUpdateContact:(nonnull TwContact*)contact {
    ++self.updatedCalls;
    self.lastUpdated = contact;
}

-(void)manager:(nonnull TwContactsManager*)manager didLoadContacts:(nonnull NSArray<TwContact *> *) contacts {
    ++self.loadedCalls;
    self.lastLoaded = contacts;
}


@end

@interface TwContactsManagerDelegateTests : XCTestCase
{
    DelegateImpl * impl;
}
@end

@implementation TwContactsManagerDelegateTests

- (void)setUp {
    impl = [[DelegateImpl alloc] init];
}

- (void)tearDown {
    impl = nil;
}

- (void)testDelegateCanBeSetAndRemoved {
    [TwContactsManager sharedManager].delegate = impl;
    XCTAssertEqual([TwContactsManager sharedManager].delegate, impl);
    
    [TwContactsManager sharedManager].delegate = nil;
    XCTAssertNil([TwContactsManager sharedManager].delegate);
}

-(void) testDelegateReceivesAdded {
    __block TwContact * addedContact = nil;

    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"add contact"];

    [TwContactsManager sharedManager].delegate = impl;
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler:^(TwContact * _Nullable contact, NSError * _Nullable error) {
        addedContact = contact;
        [expectation fulfill];
    }];
    
    [self waitForExpectations:@[expectation] timeout:10.0];
    XCTAssertEqual(impl.addedCalls, 1);
    XCTAssertNotNil(impl.lastAdded);
    XCTAssertEqualObjects(impl.lastAdded, addedContact);
}

-(void) testDelegateReceivesUpdated {
    // create the contact
    __block TwContact * addedContact = nil;
    XCTestExpectation * expectation1 = [[XCTestExpectation alloc] initWithDescription:@"add contact"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler:^ (TwContact * contact, NSError * error) {
        addedContact = contact;
        [expectation1 fulfill];
    }];
    [self waitForExpectations:@[expectation1] timeout:10.0];
    XCTAssertNotNil(addedContact);
    NSInteger identifier = addedContact.identifier;
    
    // update the contact
    [TwContactsManager sharedManager].delegate = impl;

    __block TwContact * updatedContact = nil;
    XCTestExpectation * expectation2 = [[XCTestExpectation alloc] initWithDescription:@"update contact"];
    [[TwContactsManager sharedManager] updateContactWithIdentifier:identifier first:@"newFirstName" last:@"newLastName" phone:@"newPhoneNumber" completeHandler:^ (TwContact * contact, NSError * error) {
        updatedContact = contact;
        [expectation2 fulfill];
    }];
    [self waitForExpectations:@[expectation2] timeout:10.0];
    XCTAssertEqual(impl.addedCalls, 0);
    XCTAssertNil(impl.lastAdded);
    
    XCTAssertEqual(impl.updatedCalls, 1);
    XCTAssertNotNil(impl.lastUpdated);
    XCTAssertEqualObjects(impl.lastUpdated, updatedContact);
    
}

-(void) testDelegateReceivesLoaded {
    
    __block NSArray<TwContact*>* loadedContacts = nil;
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"add contact"];
    
    [TwContactsManager sharedManager].delegate = impl;
    [[TwContactsManager sharedManager] loadContactsWithCompletionHandler:^(NSArray<TwContact *> * _Nullable contacts, NSError * _Nullable error) {
        loadedContacts = contacts;
        [expectation fulfill];
    }];
    
    [self waitForExpectations:@[expectation] timeout:10.0];
    XCTAssertEqual(impl.loadedCalls, 1);
    XCTAssertEqual(impl.addedCalls, 0);
    XCTAssertEqual(impl.updatedCalls, 0);
    XCTAssertEqualObjects(loadedContacts, impl.lastLoaded);
}
@end
