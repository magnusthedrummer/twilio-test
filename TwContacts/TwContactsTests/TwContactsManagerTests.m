//
//  TwContactsManagerTests.m
//  TwContactsTests
//
//  Created by Magnus Martikainen on 3/31/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TwContactsManager.h"
#import "TwContact.h"
#import "TwError.h"

@interface TwContactsManagerTests : XCTestCase

@end

@implementation TwContactsManagerTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

// MARK: - sharedManager tests

- (void)testSharedManagerNotNil {
    XCTAssertNotNil([TwContactsManager sharedManager]);
}

- (void)testSharedManagerReturnsSameInstance {
    TwContactsManager * first = [TwContactsManager sharedManager];
    TwContactsManager * second = [TwContactsManager sharedManager];
    XCTAssertEqual(first, second);
}

// MARK: - addContact tests

- (void)testAddContactWithNilHandlerDoesNotFail {
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler:nil];
}

- (void)testAddContactWithNilStringsDoesNotFail {
    [[TwContactsManager sharedManager] addContactWithFirst:nil last:nil phone:nil completeHandler:nil];
}

- (void)testAddContactInvokesHandler {
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"add contact"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler: ^ (TwContact * contact, NSError * error) {
        XCTAssertNotNil(contact);
        XCTAssertEqualObjects(contact.first, @"firstName");
        XCTAssertEqualObjects(contact.last, @"lastName");
        XCTAssertEqualObjects(contact.phone, @"phoneNumber");
        [expectation fulfill];
    }];
    [self waitForExpectations:@[expectation] timeout:10.0];
}

- (void)testAddContactWithSamePropertiesCreatesDifferentContacts {
    __block TwContact * contact1 = nil;
    XCTestExpectation * expectation1 = [[XCTestExpectation alloc] initWithDescription:@"add contact1"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler:^ (TwContact * contact, NSError * error) {
        contact1 = contact;
        [expectation1 fulfill];
    }];
    
    __block TwContact * contact2 = nil;
    XCTestExpectation * expectation2 = [[XCTestExpectation alloc] initWithDescription:@"add contact2"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler:^ (TwContact * contact, NSError * error) {
        contact2 = contact;
        [expectation2 fulfill];
    }];
    
    [self waitForExpectations:@[expectation1, expectation2] timeout:10.0];
    XCTAssertNotNil(contact1);
    XCTAssertNotNil(contact2);
    XCTAssertNotEqual(contact1.identifier, contact2.identifier);
}

// MARK: - updateContact tests

-(void)testUpdateContactWithNilHandlerSucceeds {
    [[TwContactsManager sharedManager] updateContactWithIdentifier: 0 first:@"newFirstName" last:@"newLastName" phone:@"newPhoneNumber" completeHandler:nil];
}

-(void)testUpdateContactWithNilStringsSucceeds {
    [[TwContactsManager sharedManager] updateContactWithIdentifier: -1 first:nil last:nil phone:nil completeHandler:nil];
}

- (void)testUpdateContactReturnsUpdatedContact {
    // create the contact
    __block TwContact * contact1 = nil;
    XCTestExpectation * expectation1 = [[XCTestExpectation alloc] initWithDescription:@"add contact"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler:^ (TwContact * contact, NSError * error) {
        contact1 = contact;
        [expectation1 fulfill];
    }];
    [self waitForExpectations:@[expectation1] timeout:10.0];
    XCTAssertNotNil(contact1);
    NSInteger identifier = contact1.identifier;
    
    // update the contact
    __block TwContact * updatedContact = nil;
    XCTestExpectation * expectation2 = [[XCTestExpectation alloc] initWithDescription:@"update contact"];
    [[TwContactsManager sharedManager] updateContactWithIdentifier:identifier first:@"newFirstName" last:@"newLastName" phone:@"newPhoneNumber" completeHandler:^ (TwContact * contact, NSError * error) {
        updatedContact = contact;
        [expectation2 fulfill];
    }];
    [self waitForExpectations:@[expectation2] timeout:10.0];
    XCTAssertNotNil(updatedContact);
    XCTAssertEqualObjects(updatedContact.first, @"newFirstName");
    XCTAssertEqualObjects(updatedContact.last, @"newLastName");
    XCTAssertEqualObjects(updatedContact.phone, @"newPhoneNumber");
    XCTAssertEqual(identifier, updatedContact.identifier);
}

- (void)testUpdateContactWithUnknownIdentifierReturnsError {
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"update contact"];

    [[TwContactsManager sharedManager] updateContactWithIdentifier:-1 first:@"newFirstName" last:@"newLastName" phone:@"newPhoneNumber"  completeHandler:^ (TwContact * contact, NSError * error) {
        XCTAssertNil(contact);
        XCTAssertNotNil(error);
        XCTAssertEqualObjects(error.domain, TwErrorDomain);
        XCTAssertEqual(error.code, TwErrorCodeContactUpdateFailed);
        XCTAssertEqualObjects(error.localizedDescription, @"Contact not found.");
        [expectation fulfill];
    }];
    [self waitForExpectations:@[expectation] timeout:10.0];
}


// MARK: - loadContact tests


- (void)testLoadContacts {
    [[TwContactsManager sharedManager] loadContacts];
}


- (void)testLoadContactsWithHandlerSucceeds {
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"load contacts"];
    [[TwContactsManager sharedManager] loadContactsWithCompletionHandler:^(NSArray<TwContact *> * _Nullable contacts, NSError * _Nullable error) {
        XCTAssertNotNil(contacts);
        XCTAssertNil(error);
        XCTAssertTrue([contacts count] > 0);
        [expectation fulfill];
    }];
    [self waitForExpectations:@[expectation] timeout:10.0];
}

- (void)testLoadContactsWithHandlerCanBeCalledMultipleTimes {
    XCTestExpectation * expectation1 = [[XCTestExpectation alloc] initWithDescription:@"load contacts"];
    [[TwContactsManager sharedManager] loadContactsWithCompletionHandler:^(NSArray<TwContact *> * _Nullable contacts, NSError * _Nullable error) {
        XCTAssertNotNil(contacts);
        XCTAssertNil(error);
        XCTAssertTrue([contacts count] > 0);
        [expectation1 fulfill];
    }];
    XCTestExpectation * expectation2 = [[XCTestExpectation alloc] initWithDescription:@"load contacts"];
    [[TwContactsManager sharedManager] loadContactsWithCompletionHandler:^(NSArray<TwContact *> * _Nullable contacts, NSError * _Nullable error) {
        XCTAssertNotNil(contacts);
        XCTAssertNil(error);
        XCTAssertTrue([contacts count] > 0);
        [expectation2 fulfill];
    }];
    [self waitForExpectations:@[expectation1, expectation2] timeout:10.0];
}

// MARK: - delegate tests

// delegate tests are in TwContactsManagerDelegateTests.m


@end
