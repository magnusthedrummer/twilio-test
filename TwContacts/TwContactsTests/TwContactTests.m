//
//  TwContactTests.m
//  TwContactsTests
//
//  Created by Magnus Martikainen on 3/31/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TwContact.h"
#import "TwContactsManager.h"

@interface TwContactTests : XCTestCase
{
    TwContact* contact;
}
@end

@implementation TwContactTests

- (void)setUp {
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"add contact"];

    // create a contact
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler: ^ (TwContact* theContact, NSError * theError) {
        self->contact = theContact;
        [expectation fulfill];
    }];
    
    [self waitForExpectations:@[expectation] timeout:10.0];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testDescriptionHasClassName {
    XCTAssertTrue([contact.description containsString:@"TwContact"]);
}

- (void)testDescriptionHasIdentifier {
    NSString * expected = [NSString stringWithFormat:@"identifier: %lu ", (unsigned long)contact.identifier];
    XCTAssertTrue([contact.description containsString:expected]);
}

- (void)testDescriptionHasFirst {
    NSString * expected = [NSString stringWithFormat:@"first: %@", contact.first];
    XCTAssertTrue([contact.description containsString:expected]);
}

- (void)testDescriptionHasLast {
    NSString * expected = [NSString stringWithFormat:@"last: %@", contact.last];
    XCTAssertTrue([contact.description containsString:expected]);
}

- (void)testDescriptionHasPhone {
    NSString * expected = [NSString stringWithFormat:@"phone: %@", contact.phone];
    XCTAssertTrue([contact.description containsString:expected]);
}

-(void)testHash {
    NSUInteger result = contact.hash;
    XCTAssertTrue(result > 31);
}

-(void)testHashForOtherContactIsDifferent {
    // create another contact
    __block TwContact * otherContact = nil;
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"add contact"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler: ^ (TwContact* theContact, NSError * theError) {
        otherContact = theContact;
        [expectation fulfill];
    }];
    
    [self waitForExpectations:@[expectation] timeout:10.0];
    XCTAssertTrue(contact.identifier != otherContact.identifier);
    
    XCTAssertNotEqual(contact.hash, otherContact.hash);
}

-(void)testIsEqualWithNilIsFalse {
    BOOL result = [contact isEqual:nil];
    XCTAssertFalse(result);
}

-(void)testIsEqualWithSelfIsTrue {
    BOOL result = [contact isEqual:contact];
    XCTAssertTrue(result);
}

-(void)testIsEqualForOtherContactIsFalse {
    // create another contact
    __block TwContact * otherContact = nil;
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"add contact"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler: ^ (TwContact* theContact, NSError * theError) {
        otherContact = theContact;
        [expectation fulfill];
    }];
    
    [self waitForExpectations:@[expectation] timeout:10.0];
    XCTAssertTrue(contact.identifier != otherContact.identifier);
    
    BOOL result = [contact isEqual:otherContact];
    XCTAssertFalse(result);
}

-(void)testIsEqualToContactWithSelfIsTrue {
    BOOL result = [contact isEqualToContact:contact];
    XCTAssertTrue(result);
}

-(void)testisEqualToContactForOtherContactIsFalse {
    // create another contact
    __block TwContact * otherContact = nil;
    XCTestExpectation * expectation = [[XCTestExpectation alloc] initWithDescription:@"add contact"];
    [[TwContactsManager sharedManager] addContactWithFirst:@"firstName" last:@"lastName" phone:@"phoneNumber" completeHandler: ^ (TwContact* theContact, NSError * theError) {
        otherContact = theContact;
        [expectation fulfill];
    }];
    
    [self waitForExpectations:@[expectation] timeout:10.0];
    XCTAssertTrue(contact.identifier != otherContact.identifier);
    
    BOOL result = [contact isEqualToContact:otherContact];
    XCTAssertFalse(result);
}


@end
