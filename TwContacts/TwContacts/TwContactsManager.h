//
//  TwContactsManager.h
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef TwContactsManager_h
#define TwContactsManager_h

#import <Foundation/Foundation.h>

// forward declarations
@class TwContact;
@protocol TwContactsManagerDelegate;


/// Block signature for callback handler invoked when contacts have been loaded.
/// If there is an error, `contacts` is `nil`. If there is no error it is all the loaded contacts.
typedef void (^ContactsLoadedCompleteHandler)(NSArray<TwContact *> * _Nullable contacts, NSError * _Nullable error);


/// Block signature for callback handler invoked when a contact has been updated or added.
/// If there is an error, `contact` is `nil`. If there is no error it is the updated/added contact.
typedef void (^ContactCompleteHandler)(TwContact * _Nullable contact, NSError * _Nullable error);


/// Twilio Contacts Manager.
///
/// Load, add and update Twilio contacts.
///
/// Use `sharedManager` to access the shared repository.
///
/// Call `loadContactsWithCompletionHandler` to retrive all the contacts.
///
/// Assign a `TwContactsManagerDelegate` to handle contact changes.
///
NS_SWIFT_NAME(ContactsManager)
@interface TwContactsManager : NSObject


/// Shared instance of the contact manager.
///
/// The implementation is thread safe.
+(nonnull instancetype)sharedManager NS_SWIFT_NAME(shared());


/// Delegate that is invoked when contacts are loaded, added and updated in the contact manager.
@property (nullable, nonatomic, weak) id <TwContactsManagerDelegate> delegate;


/// Add a new contact.
///
/// When the contact has been added, the complete handler is invoked.
-(void)addContactWithFirst:(nonnull NSString*)first last:(nonnull NSString*)last phone:(nonnull NSString*)phone completeHandler:(nullable ContactCompleteHandler)completeHandler NS_SWIFT_NAME(addContact(first:last:phone:completeHandler:));


/// Update an existing contact.
///
/// When the contact has been updated, the complete handler is invoked.
///
/// If the identifier does not match an existing contact, an error is returned in the complete handler.
///
 -(void)updateContactWithIdentifier:(NSInteger)identifier first:(nonnull NSString*)first last:(nonnull NSString*)last phone:(nonnull NSString*)phone completeHandler:(nullable ContactCompleteHandler)completeHandler NS_SWIFT_NAME(updateContact(identifier:first:last:phone:completeHandler:));


/// Load contacts into the repository.
///
/// When the operation completes, the complete handler is invoked.
-(void)loadContactsWithCompletionHandler:(nonnull ContactsLoadedCompleteHandler)completeHandler NS_SWIFT_NAME(loadContacts(completeHandler:));


/// Load contacs into the repository.
///
/// The delegate is invoked when loading completes.
-(void)loadContacts;

@end

#endif /* TwContactsManager_h */
