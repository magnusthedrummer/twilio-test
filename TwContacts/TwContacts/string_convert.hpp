//
//  string_convert.hpp
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef string_convert_h
#define string_convert_h

#import <Foundation/Foundation.h>
#include <string>

/// Safe string conversion from NSString to std::string.
///
/// - Returns: An empty string if the NSString is empty or nil.
inline std::string make_string(NSString * str) {
    return std::string([str UTF8String], [str lengthOfBytesUsingEncoding:NSUTF8StringEncoding]);
}

#endif /* string_convert_h */
