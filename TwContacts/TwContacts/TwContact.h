//
//  TwContact.h
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef TwContact_h
#define TwContact_h

#import <Foundation/Foundation.h>

/// A Twilio contact.
///
/// Contacts are created and updated using `TwContactsManager`.
NS_SWIFT_NAME(Contact)
@interface TwContact : NSObject

/// First name
@property (nonatomic, copy, readonly, nonnull) NSString * first;

/// Last name
@property (nonatomic, copy, readonly, nonnull) NSString * last;

/// Phone number
@property (nonatomic, copy, readonly, nonnull) NSString * phone;

/// Unique identifier
@property (nonatomic, readonly) NSInteger identifier;

/// Equality test.
/// - Returns `YES` if all properties of the other contact matches.
- (BOOL)isEqualToContact:(nonnull TwContact *)other NS_SWIFT_NAME(isEqual(to:));

@end


#endif /* TwContact_h */
