//
//  TwContactsManagerDelegate.h
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef TwContactsManagerDelegate_h
#define TwContactsManagerDelegate_h

@class TwContactsManager;
@class TwContact;

/// Protocol for TwContactsManager.delegate, for handling contact changes.
NS_SWIFT_NAME(ContactsManagerDelegate)
@protocol TwContactsManagerDelegate <NSObject>

@optional

/// Invoked when a new contact has been added.
///
/// **Note:** May get invoked from a background thread.
-(void)manager:(nonnull TwContactsManager*)manager didAddContact:(nonnull TwContact* )contact;


/// Invoked when a contact has been updated.
///
/// **Note:** May get invoked from a background thread.
///
/// **Note:** The updated contact is a different instance of `TwContact` (which is immutable).
/// The original TwContact instance is not updated. If cached it should be replaced with the updated instance.
-(void)manager:(nonnull TwContactsManager*)manager didUpdateContact:(nonnull TwContact*)contact;


/// Invoked when all contacts have been loaded or re-loaded.
///
/// **Note:** May get invoked from a background thread.
-(void)manager:(nonnull TwContactsManager*)manager didLoadContacts:(nonnull NSArray<TwContact *> *) contacts;

@end

#endif /* TwContactsManagerDelegate_h */
