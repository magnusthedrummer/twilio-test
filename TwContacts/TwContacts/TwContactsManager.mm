//
//  TwContactsRepository.mm
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#import "TwContactsManager.h"
#import "TwContactsManagerDelegate.h"
#import "TwContact+Native.h"

#import "TwError.h"

#include "string_convert.hpp"
#include "error_convert.hpp"

#include <tw-contacts-repository.hpp>
#include <tw-contacts-lib.hpp>

#include <memory>

using namespace std;

class repository_observer;


@interface TwContactsManager ()
{
    shared_ptr<tw::contacts_lib> pLib;
    shared_ptr<tw::contacts_repository> pRepository;
    shared_ptr<repository_observer> pObserver;
    
    // bitfield to cache which methods the delegate responds to, for speed optimization
    struct {
        unsigned int didAddContact:1;
        unsigned int didUpdateContact:1;
        unsigned int didLoadContacts:1;
    } delegateRespondsTo;
}

-(void)didObserveAddedContact:(shared_ptr<tw::contact>)contact;
-(void)didObserveUpdatedContact:(shared_ptr<tw::contact>)contact;
-(void)didObserveLoadedContacts:(std::shared_ptr<std::map<id_t, std::shared_ptr<tw::contact> > >)loaded;

@end


/// contacts_observer implementation that bridges notifications to TwContactsRepository
class repository_observer : public tw::contacts_observer {
public:
    repository_observer(shared_ptr<tw::contacts_repository> repository, TwContactsManager * manager) :
    repository(repository), manager(manager) {
    }
    
    ~repository_observer() {
        try {
            stop_observing();
        }
        catch (...) {
            // always swallow exceptions in destructors
        }
    }
    
public:
    void start_observing() {
        if (!repository) {
            return;
        }
        repository->add_observer(this);
    }
    void stop_observing() {
        if (!repository) {
            return;
        }
        repository->remove_observer(this);
    }
    
public:
    virtual void added(std::shared_ptr<tw::contact> added) {
        [manager didObserveAddedContact:added];
    }
    virtual void updated(std::shared_ptr<tw::contact> updated) {
        [manager didObserveUpdatedContact:updated];
    }
    virtual void loaded(std::shared_ptr<std::map<id_t, std::shared_ptr<tw::contact> > > loaded) {
        [manager didObserveLoadedContacts:loaded];
    }
    
private:
    shared_ptr<tw::contacts_repository> repository;
    TwContactsManager * manager;
};


@implementation TwContactsManager

@synthesize delegate;


+ (instancetype)sharedManager {
    static TwContactsManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        auto pLib = make_shared<tw::contacts_lib>();
        sharedManager = [[self alloc] initWithLib:pLib];
    });
    return sharedManager;
}


-(instancetype)initWithLib:(shared_ptr<tw::contacts_lib>) lib {
    NSAssert(lib, @"lib is null.");
    
    pLib = lib;
    pRepository = lib->get_repository();
    NSAssert(pRepository, @"repository is null");
    
    return self;
}


- (void)setDelegate:(id <TwContactsManagerDelegate>)aDelegate {
    @synchronized (self) {
        if (delegate != aDelegate) {
            delegate = aDelegate;
            
            delegateRespondsTo.didAddContact = [delegate respondsToSelector:@selector(manager:didAddContact:)];
            delegateRespondsTo.didUpdateContact = [delegate respondsToSelector:@selector(manager:didUpdateContact:)];
            delegateRespondsTo.didLoadContacts = [delegate respondsToSelector:@selector(manager:didLoadContacts:)];
        }
        if (delegate) {
            // hook up an observer
            pObserver = make_shared<repository_observer>(pRepository, self);
            pObserver->start_observing();
        } else if (pObserver) {
            // detach the observer
            pObserver->stop_observing();
            pObserver = nullptr;
        }
    }
}


-(void)didObserveAddedContact:(shared_ptr<tw::contact>)contact {
    @synchronized (self) {
        if (!delegateRespondsTo.didAddContact) {
            return;
        }
        
        // notify delegate
        TwContact * c = [[TwContact alloc] initWithContact:contact];
        [delegate manager:self didAddContact:c];
    }
}


-(void)didObserveUpdatedContact:(shared_ptr<tw::contact>)contact {
    @synchronized (self) {
        if (!delegateRespondsTo.didUpdateContact) {
            return;
        }
        
        // notify delegate
        TwContact * c = [[TwContact alloc] initWithContact:contact];
        [delegate manager:self didUpdateContact:c];
    }
}


-(void)didObserveLoadedContacts:(std::shared_ptr<std::map<id_t, std::shared_ptr<tw::contact> > >)loaded {
    @synchronized (self) {
        if (!delegateRespondsTo.didLoadContacts) {
            return;
        }

        // notify delegate
        NSMutableArray<TwContact *> * contacts = [[NSMutableArray<TwContact *> alloc] init];
        if (loaded) {
            for (auto entry : *loaded) {
                TwContact * c = [[TwContact alloc] initWithContact:entry.second];
                [contacts addObject:c];
            }
        }
        [delegate manager:self didLoadContacts:contacts];
    }
}


-(void)addContactWithFirst:(NSString*)first last:(NSString*)last phone:(NSString*)phone completeHandler:(ContactCompleteHandler)completeHandler{
    NSAssert(pRepository, @"Repository is null.");

    try {
        pRepository->add(make_string(first), make_string(last), make_string(phone), [completeHandler](shared_ptr<tw::contact> added, shared_ptr<runtime_error> failed) {
            NSError * error = nil;
            if (failed) {
                error = make_error(*failed, TwErrorCodeContactAddFailed);
            }
            
            TwContact * contact = nil;
            if (added) {
                contact = [[TwContact alloc] initWithContact:added];
            }
            
            if (completeHandler) {
                completeHandler(contact, error);
            }
        });
    }
    catch (runtime_error const & ex) {
        NSError * error = make_error(ex, TwErrorCodeContactAddFailed);
        if (completeHandler) {
            completeHandler(nullptr, error);
        }
    }
}


-(void)updateContactWithIdentifier:(NSInteger)identifier first:(NSString*)first last:(NSString*)last phone:(NSString*)phone completeHandler:(ContactCompleteHandler)completeHandler{
    NSAssert(pRepository, @"Repository is null.");

    try {
        pRepository->update(static_cast<id_t>(identifier), make_string(first), make_string(last), make_string(phone), [completeHandler](shared_ptr<tw::contact> updated, shared_ptr<runtime_error> failed) {
            NSError * error = nil;
            if (failed) {
                error = make_error(*failed, TwErrorCodeContactUpdateFailed);
            }

            TwContact * contact = nil;
            if (updated) {
                contact = [[TwContact alloc] initWithContact:updated];
            }
            
            if (completeHandler) {
                completeHandler(contact, error);
            }
        });
    }
    catch (runtime_error const & ex) {
        NSError * error = make_error(ex, TwErrorCodeContactUpdateFailed);
        if (completeHandler) {
            completeHandler(nil, error);
        }
    }
}


-(void)loadContactsWithCompletionHandler:(ContactsLoadedCompleteHandler)completeHandler {
    try {    pLib->load_all_contacts([completeHandler](shared_ptr<tw::contacts_repository::contacts_map> loaded, shared_ptr<runtime_error> failed) {
            NSError * error = nil;
            if (failed) {
                error = make_error(*failed, TwErrorCodeLoadFailed);
            }
            
            NSMutableArray<TwContact *> * contacts = [[NSMutableArray<TwContact *> alloc] init];
            if (loaded) {
                for (auto entry : *loaded) {
                    TwContact * c = [[TwContact alloc] initWithContact:entry.second];
                    [contacts addObject:c];
                }
            }
            if (completeHandler) {
                completeHandler(contacts, error);
            }
        });
    }
    catch (runtime_error const & ex) {
        NSError * error = make_error(ex, TwErrorCodeContactUpdateFailed);
        if (completeHandler) {
            completeHandler(nil, error);
        }
    }
}


-(void)loadContacts {
    NSAssert(pLib, @"pLib is null");
    pLib->load_all_contacts(nullptr);
}


@end
