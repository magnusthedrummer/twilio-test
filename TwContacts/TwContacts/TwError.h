//
//  TwError.h
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef TwError_h
#define TwError_h

#import <Foundation/Foundation.h>

/// Domain for TwContacts errors.
extern NSString * TwErrorDomain;


/// Error codes for TwContacts errors.
typedef NS_ENUM(NSInteger, TwErrorCode) {
    /// Attempt to update a contact failed.
    TwErrorCodeContactUpdateFailed,
    
    /// Attempt to add a new contact failed.
    TwErrorCodeContactAddFailed,
    
    /// Attempt to load contacts failed.
    TwErrorCodeLoadFailed
};

#endif /* TwError_h */
