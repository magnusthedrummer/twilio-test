//
//  TwContact.mm
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#import "TwContact+Native.h"

#include "string_convert.hpp"

#include <tw-contact.hpp>

#include <memory>
#include <string>
#include <functional>

using namespace std;


@implementation TwContact


-(instancetype)initWithContact:(shared_ptr<tw::contact>)contact {
    pContact = contact;
    return self;
}


-(NSInteger)identifier {
    return static_cast<NSInteger>(pContact->identifier);
}


-(NSString*)first {
    return @(pContact->first.c_str());
}


-(NSString*)last {
    return @(pContact->last.c_str());
}


-(NSString*)phone {
    return @(pContact->phone.c_str());
}


- (NSString *)description {
    return [NSString stringWithFormat: @"<%@ %p identifier: %ld first: %@ last: %@ phone: %@>", NSStringFromClass([self class]), self, (long)self.identifier, self.first, self.last, self.phone];
}


- (BOOL)isEqual:(id)other {
    if (other == self) {
        return YES;
    }
    if (!other || ![other isKindOfClass:[self class]]) {
        return NO;
    }
    return [self isEqualToContact:other];
}


- (BOOL)isEqualToContact:(TwContact *)other {
    if (self == other) {
        return YES;
    }
    
    // compare native objects
    auto pImpl = self->pContact;
    auto pOtherImpl = other->pContact;
    if (pImpl == pOtherImpl) {
        return YES;
    }
    if (pImpl == nullptr || pOtherImpl == nullptr) {
        return NO;
    }
    return ((*pImpl) == (*pOtherImpl)) ? YES : NO;
}


- (NSUInteger)hash {
    NSUInteger prime = 31;
    NSUInteger result = 1;
    if (pContact) {
        result = prime * result + hash<tw::contact>()(*pContact);
    }
    return result;
}

@end


