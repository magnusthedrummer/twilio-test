//
//  TwContact+Native.h
//  tw-contacts-swift
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef TwContactNative_h
#define TwContactNative_h

#import "TwContact.h"

#include <tw-contact.hpp>

/// Objective C++ extension for TwContact.
@interface TwContact ()
{
    std::shared_ptr<tw::contact> pContact;
}

-(instancetype)initWithContact:(std::shared_ptr<tw::contact>)contact;

@end

#endif /* TwContactNative_h */
