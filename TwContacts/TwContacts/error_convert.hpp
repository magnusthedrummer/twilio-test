//
//  error_convert.hpp
//  TwContacts
//
//  Created by Magnus Martikainen on 3/31/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef error_convert_h
#define error_convert_h

#import <Foundation/Foundation.h>
#import "TwError.h"

namespace std { class runtime_error; }

/// - Returns: `NSError` from a `runtime_error` and an error code.
inline NSError* make_error(std::runtime_error const & error, TwErrorCode code) {
    NSString * msg = @(error.what());
    return [NSError errorWithDomain:TwErrorDomain code:code userInfo:@{
          NSLocalizedDescriptionKey:msg }];
}

#endif /* error_convert_h */
