//
//  DetailViewController.swift
//  TwContactsSampleSwift
//
//  Created by Magnus Martikainen on 3/31/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

import UIKit
import TwContacts

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = "\(detail.first) \(detail.last): \(detail.phone)"
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    var detailItem: Contact? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

