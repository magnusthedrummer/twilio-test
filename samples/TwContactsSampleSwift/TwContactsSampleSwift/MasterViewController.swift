//
//  MasterViewController.swift
//  TwContactsSampleSwift
//
//  Created by Magnus Martikainen on 3/31/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

import UIKit
import TwContacts

class MasterViewController: UITableViewController, ContactsManagerDelegate {

    var detailViewController: DetailViewController? = nil
    var contacts = [Contact]()


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewContact(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        ContactsManager.shared().delegate = self;
        
        // start loading contacts
        ContactsManager.shared().loadContacts {contacts, error in
            DispatchQueue.main.async {
                if let err = error {
                    self.showError(err.localizedDescription)
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    @objc
    func insertNewContact(_ sender: Any) {
        let first = ["Ford", "Zaphod", "Trillian", "Arthur", "Marvin"].randomElement() ?? "Magnus"
        let last = ["Prefect", "Beeblebrox", "Astra", "Dent", "Android"].randomElement() ?? "Martikainen"
        let phone = "(\(Int.random(in: 100..<1000)))-\(Int.random(in: 100..<1000)) \(Int.random(in: 1000..<10000))"
        ContactsManager.shared().addContact(first:first, last:last, phone:phone){ contact, error in
            DispatchQueue.main.async {
                if let err = error {
                    self.showError(err.localizedDescription)
                }
            }
        }
    }
    
    // MARK: - Errors
    
    func showError(_ message:String) {
        let alertController = UIAlertController(title:"Error", message:message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title:"Dismiss", style:.default))
        present(alertController, animated: true, completion: nil)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let contact = contacts[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = contact
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let contact = contacts[indexPath.row]
        cell.textLabel!.text = "\(contact.last), \(contact.first)"
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            contacts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    func setContacts(_ contacts: [Contact]) {
        DispatchQueue.main.async {
            // sort alphabetically by last name
            self.contacts = contacts.sorted { $0.last < $1.last }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - ContactsManagerDelegate
    
    func manager(_ manager: ContactsManager, didLoad contacts: [Contact]) {
        setContacts(contacts);
    }
    
    
    func manager(_ manager: ContactsManager, didAdd contact: Contact) {
        var contacts = self.contacts;
        contacts.append(contact);
        setContacts(contacts)
    }
    
    func manager(_ manager: ContactsManager, didUpdate contact: Contact) {
        var contacts = self.contacts;
        
        // replace the updated contact
        contacts.removeAll { $0.identifier == contact.identifier}
        contacts.append(contact)
        
        setContacts(contacts)
    }

}

