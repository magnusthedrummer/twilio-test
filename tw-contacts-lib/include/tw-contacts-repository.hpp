//
//  tw-contacts-repository.hpp
//  tw-contacts-lib
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef tw_contacts_repository_hpp
#define tw_contacts_repository_hpp

#include "tw-contact.hpp"
#include "tw-contacts-observer.hpp"

#include <mutex>
#include <set>
#include <map>

namespace tw {
    
    /// Repository for Twilio contacts.
    ///
    /// Changes to the repository are communicated through observers.
    ///
    /// All operations on the repository are thread safe.
    class contacts_repository {
    public:
        /// Contacts for lookup by `identifier`.
        typedef std::map<id_t, std::shared_ptr<contact> > contacts_map;
        
        /// Callback function used when updating/adding a contact.
        ///
        /// If the operation is successful the contact is available in the `contact` parameter.
        ///
        /// If an error occurs it is available in the `error` parameter.
        typedef std::function<void(std::shared_ptr<contact> contact, std::shared_ptr<std::runtime_error> error)> contact_handler;
        
        /// Callback function used when loading contacts.
        ///
        /// If the operation is successful loaded contacts are available in the `contacts` parameter.
        ///
        /// If an error occurs it is available in the `error` parameter.
        typedef std::function<void(std::shared_ptr<contacts_map> contacts, std::shared_ptr<std::runtime_error> error)> contacts_loaded_handler;
        
    public:
        /// Create an empty repository.
        contacts_repository();
        
        /// Create a repository with a set of contacts.
        contacts_repository(std::shared_ptr<contacts_map> contacts);

    public:
        /// Add an observer to the repository.
        ///
        /// If the observer is already in the repository, it is not added again.
        ///
        /// **Note:* The observer may get invoked on a different thread.
        ///
        /// It is the responsibility of the caller to remove the observer before it is destroyed.
        void add_observer(contacts_observer * observer);
        
        /// Remove an existing observer from the repository.
        ///
        /// If the observer is not in the repository, nothing happens.
        void remove_observer(contacts_observer * observer);
        
    public:
        /// Add a new contact to the repository and notify observers.
        ///
        /// - Returns: the added contact in the handler
        ///
        /// **Note:* The handler may get invoked on a different thread.
        void add(std::string const & first, std::string const & last, std::string const & phone, contact_handler handler);

        
        /// Update an existing contact in the repository and notify observers.
        ///
        /// - Param: identifier - unique identifier for the contact being updated.
        ///
        /// - Returns: the updated contact in the handler
        /// **Note:** The returned contact is a new instance. The original instance is not updated.
        ///
        /// - Returns: a `std::runtime_error` in the handler if a contact with the specified identifier is not found in the repository.
        ///
        /// **Note:* The handler may get invoked on a different thread.
        void update(id_t identifier, std::string const & first, std::string const & last, std::string const & phone, contact_handler handler);
        
        
        /// Load contacts into the repository from a JSON string and notify observers.
        ///
        /// - Param: json - valid JSON string with an array of contacts, in the form:
        /// ```js
        /// [
        ///    {"first":"John", "last":"Doe", "phone":"+14085554199"},
        /// ]
        /// ```
        ///
        /// - Returns: the loaded contacts in the handler.
        /// If loading fails an error is returned in the handler.
        ///
        /// **Note:* The handler may get invoked on a different thread.
        void load_json_contacts(std::string const & json, contacts_loaded_handler handler);
        
    private:
        std::shared_ptr<contacts_map> parse_json(std::string const & json);
        
        void notify_added(std::shared_ptr<contact> added);
        void notify_updated(std::shared_ptr<contact> updated);
        void notify_loaded(std::shared_ptr<contacts_map> loaded);
        
        id_t make_identifier() const;
        
    private:
        typedef std::set<contacts_observer*> observer_set;
        observer_set observers;
        std::mutex observers_mutex;
        
        std::shared_ptr<contacts_map> contacts;
        std::mutex contacts_mutex;
    };

}

#endif /* tw_contacts_repository_hpp */
