//
//  tw-contacts-lib.hpp
//  tw-contacts-lib
//
//  Created by Magnus Martikainen on 3/29/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef tw_contacts_lib_hpp
#define tw_contacts_lib_hpp

#include "tw-contacts-repository.hpp"

#include <mutex>

namespace tw {
    
    /// C++ Library for Twilio Contacts.
    ///
    /// Contacts are accessed via `get_repository()`.
    ///
    /// To load contacts into the repository, call `loadContacts()`.
    ///
    /// The library is thread safe.
    class contacts_lib {
    public:
        contacts_lib();
        
    public:
        std::shared_ptr<contacts_repository> get_repository();
        
        void load_all_contacts(contacts_repository::contacts_loaded_handler handler);
        
    private:
        std::shared_ptr<contacts_repository> repository;
        std::mutex repository_mutex;
    };
    
    
} // namespace tw

#endif /* tw_contacts_lib_hpp */
