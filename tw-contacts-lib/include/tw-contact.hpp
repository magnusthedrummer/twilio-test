//
//  tw-contact.hpp
//  tw-contacts-lib
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef tw_contact_hpp
#define tw_contact_hpp

#include <string>
#include <memory>

namespace tw {
    /// integer identifiers for contacts
    typedef id_t size_type;
    
    /// Represents a contact, with first name, last name and phone number.
    ///
    /// The contact is immutable and thread safe.
    ///
    /// Contacts are created through the repository. They cannot be updated - only replaced.
    class contact {
    public:
        contact(id_t identifier, std::string const & first, std::string const & last, std::string const & phone);

    public:
        id_t const identifier;
        std::string const first;
        std::string const last;
        std::string const phone;
        
    public:
        bool operator==(contact const & other) const;
    };
}

/// hash function specialization
namespace std {
    template <>
    class hash<::tw::contact>{
    public :
        size_t operator()(const ::tw::contact &contact) const {
            size_t prime = 31;
            size_t result = 1;
            auto hash_string = hash<string>();
            result = prime * result + hash<id_t>()(contact.identifier);
            result = prime * result + hash_string(contact.first);
            result = prime * result + hash_string(contact.last);
            result = prime * result + hash_string(contact.phone);
            return result;
        }
    };
}

#endif /* tw_contact_hpp */
