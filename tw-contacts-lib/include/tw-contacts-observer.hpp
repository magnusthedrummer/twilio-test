//
//  tw-contacts-observer.hpp
//  tw-contacts-lib
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#ifndef tw_contacts_observer_hpp
#define tw_contacts_observer_hpp

#include <memory>
#include <map>

#include "tw-contact.hpp"

namespace tw {
    
class contacts_observer {
public:
    virtual void added(std::shared_ptr<contact> added) = 0;
    virtual void updated(std::shared_ptr<contact> updated) = 0;
    virtual void loaded(std::shared_ptr<std::map<id_t, std::shared_ptr<contact> > > loaded) = 0;
};

}


#endif /* tw_contacts_observer_hpp */
