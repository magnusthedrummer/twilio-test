//
//  tw-contacts-lib.cpp
//  tw-contacts-lib
//
//  Created by Magnus Martikainen on 3/29/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#include "../include/tw-contacts-lib.hpp"

using namespace std;

extern "C" std::string contacts;


namespace tw {

    contacts_lib::contacts_lib() {
    }
    
    
    std::shared_ptr<contacts_repository> contacts_lib::get_repository() {
        lock_guard<mutex> guard(repository_mutex);
        if (!repository) {
            repository = make_shared<contacts_repository>();
        }
        return repository;
    }
    
    
    void contacts_lib::load_all_contacts(contacts_repository::contacts_loaded_handler handler) {
        auto repository = get_repository();
        repository->load_json_contacts(contacts, handler);
    }
    
    
}
