//
//  tw-contact.cpp
//  tw-contacts-lib
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#include "tw-contact.hpp"

using namespace std;


namespace tw {

    contact::contact(id_t identifier, std::string const & first, std::string const & last, std::string const & phone)
    : identifier(identifier), first(first), last(last), phone(phone) {
    }
    
    
    bool contact::operator==(contact const & other) const {
        if (identifier != other.identifier) {
            return false;
        }
        if (first != other.first) {
            return false;
        }
        if (last != other.last) {
            return false;
        }
        if (phone != other.phone) {
            return false;
        }
        return true;
    }
    
}
