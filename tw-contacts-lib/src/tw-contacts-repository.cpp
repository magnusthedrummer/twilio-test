//
//  tw-contacts-repository.cpp
//  tw-contacts-lib
//
//  Created by Magnus Martikainen on 3/30/19.
//  Copyright © 2019 MagnusTheDrummer. All rights reserved.
//

#include "../include/tw-contacts-repository.hpp"

#include <json11.hpp>

#include <map>
#include <mutex>

using namespace std;

namespace tw {
    
    contacts_repository::contacts_repository() {
        contacts = make_shared<contacts_map>();
    }
    
    
    contacts_repository::contacts_repository(shared_ptr<contacts_map> contacts)
    : contacts(contacts) {
    }
    
    
    void contacts_repository::add_observer(contacts_observer * observer) {
        lock_guard<mutex> guard(observers_mutex);
        observers.insert(observer);
    }
    
    
    void contacts_repository::remove_observer(contacts_observer * observer) {
        lock_guard<mutex> guard(observers_mutex);
        observers.erase(observer);
    }
    
    
    void contacts_repository::notify_added(shared_ptr<contact> added) {
        lock_guard<mutex> guard(observers_mutex);
        for (auto o : observers) {
            o->added(added);
        }
    }
    
    
    void contacts_repository::notify_updated(shared_ptr<contact> updated) {
        lock_guard<mutex> guard(observers_mutex);
        for (auto o : observers) {
            o->updated(updated);
        }
    }
    

    void contacts_repository::notify_loaded(shared_ptr<contacts_map> loaded) {
        lock_guard<mutex> guard(observers_mutex);
        for (auto o : observers) {
            o->loaded(loaded);
        }
    }
    
    
    /// Parse a JSON string of a contacts array into a contacts map.
    ///
    /// - Throws: a `runtime_error` if contact parsing fails.
    shared_ptr<contacts_repository::contacts_map> contacts_repository::parse_json(string const & json) {
        using namespace json11;
        
        string err;
        Json parsed = Json::parse(json, err);
        if (!err.empty()) {
            throw runtime_error("Failed to parse JSON: " + err);
        }
        if (!parsed.is_array()) {
            throw runtime_error("JSON is not an array.");
        }
        
        auto result = make_shared<contacts_map>();
        id_t identifier = 0;
        for (auto o : parsed.array_items()) {
            auto c = make_shared<contact>(identifier, o["first"].string_value(), o["last"].string_value(), o["phone"].string_value());
            (*result)[identifier] = c;
            ++identifier;
        }
        return result;
    }
    
    
    void contacts_repository::load_json_contacts(string const & json, contacts_loaded_handler handler) {
        shared_ptr<contacts_map> loaded;
        shared_ptr<runtime_error> error;
        try {
            loaded = parse_json(json);
            // lock scope
            {
                lock_guard<mutex> guard(contacts_mutex);
                contacts = loaded;
            }
            if (loaded) {
                notify_loaded(loaded);
            }
        }
        catch (runtime_error const & err) {
            error = make_shared<runtime_error>(err);
        }
        catch (...) {
            // responses would normally be handled on a background thread, in which case all exceptions must be trapped, to ensure an error is sent to the handler
            error = make_shared<runtime_error>("Unknown exception.");
        }
        
        // invoke handler
        if (handler) {
            handler(loaded, error);
        }
    }
    
    
    void contacts_repository::add(std::string const & first, std::string const & last, std::string const & phone, contact_handler handler) {
        shared_ptr<contact> added;
        shared_ptr<runtime_error> error;
        try {
            // lock scope
            {
                lock_guard<mutex> guard(contacts_mutex);
                auto identifier = make_identifier();
                added = make_shared<contact>(identifier, first, last, phone);
                (*contacts)[identifier] = added;
            }
            if (added) {
                notify_added(added);
            }
        }
        catch (runtime_error const & err) {
            error = make_shared<runtime_error>(err);
        }
        catch (...) {
            // responses would normally be handled on a background thread, in which case all exceptions must be trapped, to ensure an error is sent to the handler
            error = make_shared<runtime_error>("Unknown exception.");
        }
        
        // invoke handler
        if (handler) {
            handler(added, error);
        }
    }
    
    
    /// - Returns: A new, unique identifier for a contact.
    ///
    /// Should only be called while holding a lock on the `contacts_mutex`.
    id_t contacts_repository::make_identifier() const {
        // NOTE: since we don't support removing contacts, we can simply use the number of contacts as the next identifier. (Normally this ID would come from the server.)
        return static_cast<id_t>(contacts->size());
    }
    
    
    void contacts_repository::update(id_t identifier, std::string const & first, std::string const & last, std::string const & phone, contact_handler handler) {
        shared_ptr<contact> updated;
        shared_ptr<runtime_error> error;
        try {
            // lock scope
            {
                lock_guard<mutex> guard(contacts_mutex);
                auto found = (*contacts)[identifier];
                if (found) {
                    updated = make_shared<contact>(identifier, first, last, phone);
                    (*contacts)[identifier] = updated;
                }
                else {
                    error = make_shared<runtime_error>("Contact not found.");
                }
            }
            if (updated) {
                notify_updated(updated);
            }
        }
        catch (runtime_error const & err) {
            error = make_shared<runtime_error>(err);
        }
        catch (...) {
            // responses would normally be handled on a background thread, in which case all exceptions must be trapped, to ensure an error is sent to the handler
            error = make_shared<runtime_error>("Unknown exception.");
        }
        
        // invoke handler
        if (handler) {
            handler(updated, error);
        }
    }
    
} // namespace tw

